# COMP3931 Individual Project 
* Topic: Object Detection using Deep Learning – Home Decor Tool
* Name: Hritk Mehta 
* Username: sc19h2m
* Student ID: 201346543

## This repository contains all the code and resources required to test and run the various elements of this project.

## 1) Deep learning - Colab Notebooks
The [Colab Notebooks](https://gitlab.com/sc19h2m/fyp/-/tree/master/Colab%20Notebooks) directory contains the Google Colab - Jupyter Notebooks for the models developed; Mask R-CNN and YOLOv4.

They notebooks contain links to open them in Colab, alternatively, you can click the following links to access them on Colab.

Mask R-CNN:
https://colab.research.google.com/github/hritik-7/FYP/blob/main/FYP_MRCNN.ipynb

YOLOv4:
https://colab.research.google.com/github/hritik-7/FYP/blob/main/FYP_YOLOv4.ipynb

These notebooks contain instructions on how to set them up, train a model with different parameters, evaluate the new model, evaluate the best performing network and make predictions on pre-existing or new images.


## 2) Object Detection Flask App
The [Image Processing Tool](https://gitlab.com/sc19h2m/fyp/-/tree/master/Image%20Processing%20Tool) directory contains all the relevant files and folders to run the app server on a local machine.

The following instructions assume you have Anaconda installed on your system.

* Clone the repository on your machine.
```bash
git clone https://gitlab.com/sc19h2m/fyp.git
```
* Move into the [Image Processing Tool](https://gitlab.com/sc19h2m/fyp/-/tree/master/Image%20Processing%20Tool) directory
```bash
cd "Image Processing Tool"
```
* Create and activate the anaconda environment
```bash
# CPU
conda env create -f conda-cpu.yml
conda activate yolov4-cpu

# GPU
conda env create -f conda-gpu.yml
conda activate yolov4-gpu
```
* Install the dependencies
```bash
# CPU
pip install -r requirements.txt

# GPU
pip install -r requirements-gpu.txt
```
* Run the app
```bash
python app.py
```
* Open the server URL (http://127.0.0.1:5000) using a web browser (this loads the YOLOv4 model so it may take up to 15-20 seconds)

### How it should look (click on the picture to redirect to a video demonstrating the app):
[![Video showing the working of the app](https://gitlab.com/sc19h2m/fyp/-/raw/master/Image%20Processing%20Tool/static/images/UI.png)](https://youtu.be/9kmGgWvOdgs)

* You can now upload an image containing a window, select the fabric and click proceed to see the magic happen.

### Exampe of what the result may resemble:
[![Video showing the working of the app](https://gitlab.com/sc19h2m/fyp/-/raw/master/Image%20Processing%20Tool/static/images/UI_2.png)](https://youtu.be/9kmGgWvOdgs)
