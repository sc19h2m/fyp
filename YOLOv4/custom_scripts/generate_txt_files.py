import argparse
import logging
import os
import sys
import traceback
from pathlib import Path
from typing import Dict, Any
import numpy as np


from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import asarray
from matplotlib import pyplot
from mrcnn.visualize import display_instances



from dataset_manager.floor_plan import FloorPlan
from dataset_manager.render import render_room_vertices_on_panos
from dataset_manager.custom_scripts.render_windows import render_windows
from tqdm import tqdm

IMAGE_DIM = 512


def load_dataset(img_dirs, is_train = False):
    # Define the class

    panos_to_visualize = ["primary","secondary"]
    geometry_type = "visible"

    id = 0
    file_num = 0
    start_dir = 60
    # if is_train:
    #     start_dir = 0

    input_files_list = []
    print("NUMBER_OF_IMAGE_FILES" + str(img_dirs))
    for i in range(start_dir, img_dirs):
        if i == 1456:
            continue
        fname = "../zind_dataset/"
        fname += str(i).zfill(4)+"/zind_data.json"
        # fname += "1000/zind_data.json"
        input_files_list.append(fname)

    num_failed = 0
    num_success = 0
    failed_tours = []

    for input_file in tqdm(input_files_list, desc="Validating ZInD data"):
        print(input_file)
        # Try loading and validating the file.
        print('hellp')
        file_num += 1
        try:
            # print("################################"+str(input_file))
            print(str(input_file))
            zillow_floor_plan = FloorPlan(input_file)
            # print("################################")
            input_folder = os.path.join(str(Path(input_file).parent))
            cnt = 0
            # print("################################")
            for pano_type in panos_to_visualize:
                panos_list = zillow_floor_plan.panos_layouts[geometry_type][pano_type]
                # print(panos_list)
                for pano_data in panos_list:
                    # print("**************************")
                    pano_id = pano_data["pano_id"]
                    # print("*************1111111111*************")

                    pano_image_path = os.path.join(input_folder,  pano_data["image"].split('/')[0], pano_data["image"].split('/')[1])
                    # print("*************222222222*************")

                    bboxes_pixel_coos, pano_width, pano_height = render_windows(
                        input_folder=zillow_floor_plan.input_folder,
                        pano_data=pano_data
                    )
                    # print("*************3333333333333*************")

                    if bboxes_pixel_coos == -1:
                        continue
                    print("\nProcessing Pano: " +str(id))
                    size = len(pano_image_path)
                    # print(pano_image_path[:size - 4])
                    # print(pano_image_path[3:])
                    train_fname = os.path.join("/../../../../drive/MyDrive/Colab Notebooks/FYP/", pano_image_path[3:])
                    fill_train_file(train_fname, is_train)
                    # fill_train_file(pano_image_path[3:])
                    # txt_fname = os.path.join(pano_image_path[:size - 4])
                    txt_fname = pano_image_path[:size - 4] + ".txt"
                    # print(txt_fname)
                    with open(txt_fname, 'w') as f:
                        for bbox in bboxes_pixel_coos:
                            # if bbox[1][1] < bbox[2][1]:
                            #     print("it exists!!!!!******************************")
                            #     cnt += 1
                            width, height, center_x, center_y = extract_info(bbox)
                            width /= pano_width
                            height /= pano_height
                            center_x /= pano_width
                            center_y /= pano_height
                            # print("width" + str(width))
                            # print("height" + str(height))
                            # print("center_x" + str(center_x))
                            # print("center_y" + str(center_y))
                            # f.write("0 %0.6f %0.6f %0.6f %0.6f", % center_x, % center_y, % width, % height)
                            f.write(f"0 %0.6f %0.6f %0.6f %0.6f \n" % (center_x, center_y, width, height))
                    # print("\n\n")
                    # print(bboxes_pixel_coos)
                    # print("\n\n\n")
                    # print(pano_image_path)
                    # print(id)
                    id += 1
            # print("**************************************" + str(cnt))
        except Exception as ex:
            print("@@@@@@@@@@@@@@@@@@@@@")
            print(ex)
            failed_tours.append(str(Path(input_file).parent.stem))
            num_failed += 1
            track = traceback.format_exc()
            continue



# load an image reference
def image_reference(self, image_id):
    info = self.image_info[image_id]
    return info['path']

def extract_info(bbox):

    width = abs(bbox[0][0] - bbox[1][0])
    height = 0.0

    center_x = lower(bbox[0][0], bbox[1][0]) + (width/2)
    lowest_y = lower(bbox[2][1], bbox[3][1])
    highest_y = higher(bbox[0][1], bbox[1][1])
    center_y = lowest_y + ((highest_y - lowest_y)/2)
    height = highest_y - lowest_y

    return width, height, center_x, center_y



def lower(a, b):
    if a < b:
        return a
    return b

def higher(a, b):
    if a > b:
        return a
    return b

def fill_train_file(path, is_train = False):
    if is_train:
        with open('train.txt', 'a') as f:
            f.write(path + "\n")
    else:
        with open('test.txt', 'a') as f:
            f.write(path + "\n")

def main():
    parser = argparse.ArgumentParser(
        description="Visualize & validate Zillow Indoor Dataset (ZInD)."
    )

    parser.add_argument(
        "--images",
        "-i",
        help="Number of image folders.",
        type=int,
    )

    parser.add_argument(
        "--test",
        "-t",
        help="Number of image folders for testing.",
        type=int,
    )


    args = parser.parse_args()
    if args.images:
        load_dataset(args.images, True)
    if args.test:
        load_dataset(args.test)



if __name__ == "__main__":
    main()
