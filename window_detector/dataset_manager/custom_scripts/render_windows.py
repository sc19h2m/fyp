"""
This module contains the window rendering route.
"""
import argparse
import itertools
import logging
import os
import sys
# sys.path.append("..")
from typing import List, Dict, Any
from pathlib import Path

import cv2
import numpy as np
from ..pano_image import PanoImage
from ..transformations import TransformationSpherical, Transformation3D
from ..utils import Polygon, PolygonType
from ..floor_plan import FloorPlan

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
LOG = logging.getLogger(__name__)

# Default parameters when drawing ZInD floor plans.
DEFAULT_LINE_THICKNESS = 4
DEFAULT_RENDER_RESOLUTION = 2048

# Polygon colors when we render the floor map as a JPG image.
POLYGON_COLOR = {
    PolygonType.WINDOW: (0, 255, 255),  # Cyan
}


def render_windows(pano_data: Dict[str, Any], *, input_folder: str, visualise = False, output_folder = False):
    """Render window information on the panoramas.

    :parameter panos_list: The list of collected per-pano data.
    :parameter input_folder: The input folder, used to locate the pano textures.

    :return bboxes_pixel_coos: Numpy array containing bounding box coordinates
    :return pano_width: pano width
    :return pano_height: pano_height
    """


    PolygonTypeMapping = {
        "windows": PolygonType.WINDOW,
    }

    pano_id = pano_data["pano_id"]
    pano_image_path = os.path.join(input_folder, pano_data["image"])
    pano_image = PanoImage.from_file(pano_image_path)
    pano_width = pano_image.width
    pano_height = pano_image.height

    transform = Transformation3D(
        camera_height=pano_data["camera_height"],
        ceiling_height=pano_data["ceiling_height"],
    )

    # Setting the window/door/opening mode
    type = "windows"

    # Skip if there are no windows
    if type not in pano_data:
        return -1,-1,-1

    vertices = pano_data[type]

    # Skip if there are no windows
    if len(vertices) == 0:
        return -1,-1,-1

    # Each WDO is represented by three continuous elements:
    # (left boundary x,y); (right boundary x,y); (bottom boundary z, top boundary z)
    assert len(vertices) % 3 == 0
    num_wdo = len(vertices) // 3

    bboxes_pixel_coos = []

    for i in range(num_wdo):

        bottom_z = vertices[i * 3 + 2][0]

        top_z = vertices[i * 3 + 2][1]

        # bbox contains four points at bottom left, bottom right, top right, top left
        bbox = np.array(
            [
                [
                    vertices[i * 3][0],
                    vertices[i * 3][1],
                    bottom_z,

                ],
                [
                    vertices[i * 3 + 1][0],
                    vertices[i * 3 + 1][1],
                    bottom_z,
                ],
                [
                    vertices[i * 3 + 1][0],
                    vertices[i * 3 + 1][1],
                    top_z,
                ],
                [
                    vertices[i * 3][0],
                    vertices[i * 3][1],
                    top_z,
                ],
            ]
        )


        bbox = TransformationSpherical.normalize(bbox)

        if visualise:
            visualise_windows(pano_image, bbox, POLYGON_COLOR[PolygonTypeMapping[type]])

        bbox = TransformationSpherical.cartesian_to_pixel(bbox, pano_width)

        # Exclude inaccurate bounding boxes
        if abs(bbox[0][0] - bbox[1][0]) > 1500:
            continue

        bboxes_pixel_coos.append(bbox)

    if visualise:
        # show_image(pano_image)
        save_image(pano_image, pano_id, output_folder)
        return

    return bboxes_pixel_coos, pano_width, pano_height


def visualise_windows(pano_image, bbox, color):
    pano_image.draw_vertical_line(
        (bbox[0], bbox[3]),
        color=color,
    )
    pano_image.draw_vertical_line(
        (bbox[1], bbox[2]),
        color=color,
    )
    pano_image.draw_spherical_line(
        bbox[0],
        bbox[1],
        color=color,
    )
    pano_image.draw_spherical_line(
        bbox[3],
        bbox[2],
        color=color,
    )

def show_image(image):
    """Display image."""
    image_bgr_cv = cv2.cvtColor(image.opencv_image, cv2.COLOR_RGB2BGR)
    cv2.imshow("img",image_bgr_cv)
    cv2.waitKey(0)

def save_image(image, id, output_folder):
    """Save image."""
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    fout = os.path.join(output_folder, "{}_layout.jpg".format(id))
    image.write_to_file(fout)

def visualiser(fname, fout):
    """Visualise windows image."""
    panos_to_visualize = ["primary","secondary"]
    geometry_type = "visible"
    zillow_floor_plan = FloorPlan(fname)
    input_folder = os.path.join(str(Path(fname).parent))
    for pano_type in panos_to_visualize:

        panos_list = zillow_floor_plan.panos_layouts[geometry_type][pano_type]

        for pano_data in panos_list:

            pano_id = pano_data["pano_id"]
            pano_image_path = os.path.join(input_folder,  pano_data["image"].split('/')[0], pano_data["image"].split('/')[1])

            render_windows(input_folder=zillow_floor_plan.input_folder,
                pano_data=pano_data,
                visualise=True,
                output_folder=fout
            )
