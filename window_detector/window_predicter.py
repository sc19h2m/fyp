import argparse
import logging
from os import listdir
import os
import sys
import traceback
from pathlib import Path
from typing import Dict, Any
import shutil

from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import asarray
from numpy import expand_dims
from numpy import mean
from mrcnn.utils import Dataset
from matplotlib import pyplot
from mrcnn.visualize import display_instances
from mrcnn.utils import extract_bboxes
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.utils import compute_ap
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image
from matplotlib.patches import Rectangle
import skimage.color
import skimage.io
import skimage.transform
from numpy import random
import pickle
import time

FILE_RANGE_BEGINNING = 0
IMAGES_TO_PREDICT = 10



from dataset_manager.floor_plan import FloorPlan
from dataset_manager.custom_scripts.render_windows import render_windows
from tqdm import tqdm

class WindowDataset(Dataset):
    def load_dataset(self, input_dir, is_train  = True,  num_img_dir = 5, num_train_dir = 4, count = 200):
        # Define the class
        self.add_class("dataset", 1, "windows")

        panos_to_visualize = ["primary","secondary"]
        geometry_type = "visible"

        id = 0
        file_num = 0

        input_files_list = []
        for i in range(FILE_RANGE_BEGINNING, FILE_RANGE_BEGINNING + num_img_dir):
            if i == 1456:
                continue
            fname = input_dir
            fname += str(i).zfill(4)+"/zind_data.json"
            input_files_list.append(fname)


        num_failed = 0
        num_success = 0
        failed_tours = []

        description = "Looping through dataset directories (will stop at image count = " + str(count) + ")"

        for input_file in tqdm(input_files_list, desc=description):
            # Try loading and validating the file.
            file_num += 1

            if is_train and file_num >= num_train_dir:
                break
            elif not is_train and file_num < num_train_dir:
                continue

            try:

                zillow_floor_plan = FloorPlan(input_file)
                input_folder = os.path.join(str(Path(input_file).parent))
                for pano_type in panos_to_visualize:
                    panos_list = zillow_floor_plan.panos_layouts[geometry_type][pano_type]

                    for pano_data in panos_list:
                        pano_id = pano_data["pano_id"]

                        pano_image_path = os.path.join(input_folder,  pano_data["image"].split('/')[0], pano_data["image"].split('/')[1])

                        bboxes_pixel_coos, pano_width, pano_height = render_windows(
                            input_folder=zillow_floor_plan.input_folder,
                            pano_data=pano_data
                        )
                        if bboxes_pixel_coos == -1:
                            continue


                        self.add_image('dataset', image_id=id, path=pano_image_path,image_width=pano_width, image_height=pano_height, bboxes=bboxes_pixel_coos)
                        id += 1
                        if id >= count:
                          return



            except Exception as ex:
                print("@@@@@@@@@@@@@@@@@@@@@")
                print(ex)
                failed_tours.append(str(Path(input_file).parent.stem))
                num_failed += 1
                track = traceback.format_exc()
                continue


    # load the masks for an image
    def load_mask(self, image_id):
        # get details of image
        info = self.image_info[image_id]

        print("Loading Mask for image: " + str(image_id))

        boxes = info['bboxes']
        w = info['image_width']
        h = info['image_height']
        # create one array for all masks, each on a different channel
        masks = zeros([h, w, len(boxes)], dtype='uint8')

        # create masks
        class_ids = list()

        for i in range(len(boxes)):

            box = boxes[i]
            # print(box[2][1])
            if box[2][1]<box[3][1] and box[1][1]>box[0][1]:
                row_s, row_e = int(box[2][1]), int(box[1][1])
            elif box[2][1]<box[3][1] and box[0][1]>box[1][1]:
                row_s, row_e = int(box[2][1]), int(box[0][1])
            elif box[3][1]<box[2][1] and box[1][1]>box[0][1]:
                row_s, row_e = int(box[3][1]), int(box[1][1])
            else:
                row_s, row_e = int(box[3][1]), int(box[0][1])

            if box[3][0] > box[2][0]:
                col_s, col_e = int(box[2][0]), int(box[3][0])
            else:
                col_s, col_e = int(box[3][0]), int(box[2][0])

            masks[row_s:row_e, col_s:col_e, i] = 1

            class_ids.append(self.class_names.index('windows'))


        return masks, asarray(class_ids, dtype='int32')

    # load an image reference
    def image_reference(self, image_id):
        info = self.image_info[image_id]
        return info['path']

# define the prediction configuration
class PredictionConfig(Config):
    # define the name of the configuration
    NAME = "window_cfg"
    # number of classes (background + kangaroo)
    NUM_CLASSES = 1 + 1
    # simplify GPU config
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    # IMAGE_MAX_DIM = 256
    @classmethod
    def set_max_dim(cls, image_dim):
        cls.IMAGE_MAX_DIM = image_dim
        print("PredictionConfig.IMAGE_MAX_DIM set to " + str(cls.IMAGE_MAX_DIM) + "x" + str(cls.IMAGE_MAX_DIM))

    @classmethod
    def get_max_dim(cls):
        print("PredictionConfig.IMAGE_MAX_DIM = " + str(cls.IMAGE_MAX_DIM))

# calculate the mAP for a model on a given dataset
def evaluate_model(dataset, model, cfg):
    APs = list()
    for image_id in dataset.image_ids:
        # load image, bounding boxes and masks for the image id
        image, _, gt_class_id, gt_bbox, gt_mask = load_image_gt(dataset, cfg, image_id,
        use_mini_mask=False)
        # convert pixel values
        scaled_image = mold_image(image, cfg)
        # convert image into one sample
        sample = expand_dims(scaled_image, 0)
        # make prediction
        yhat = model.detect(sample, verbose=0)
        # extract results for first sample
        r = yhat[0]
        # calculate statistics, including AP
        AP, _, _, _ = compute_ap(gt_bbox, gt_class_id, gt_mask, r["rois"], r["class_ids"],
        r["scores"], r['masks'])
        # store
        APs.append(AP)
    # calculate the mean AP across all images
    mAP = mean(APs)
    return mAP

def load_custom_image(image_path):
    """Load the specified image and return a [H,W,3] Numpy array.
    """
    # Load image
    image = skimage.io.imread(image_path)
    # If grayscale. Convert to RGB for consistency.
    if image.ndim != 3:
        image = skimage.color.gray2rgb(image)
    # If has an alpha channel, remove it for consistency
    if image.shape[-1] == 4:
        image = image[..., :3]
    return image

# plot a number of photos with ground truth and predictions
def plot_actual_vs_predicted(dataset, model, cfg, weights_file, n_images=5, custom_image_path=None):
    wf = weights_file.split(".")
    output_dir_name = "window_predictions"
    output_dir = Path(output_dir_name)
    if output_dir.exists() and output_dir.is_dir():
      shutil.rmtree(output_dir)
    os.mkdir(output_dir_name)
    total_time = 0.0
    count = 0
    min_time = 500.0
    max_time = 0.0

    if custom_image_path:
      n_images = 1

    # load image and mask
    for i in range(n_images):
        fig_name = wf[0]
        # load the image and mask
        if not custom_image_path:
            image = dataset.load_image(i)
            mask, _ = dataset.load_mask(i)
        else:
            image = load_custom_image(custom_image_path)

        # Time the detection
        start_time = time.time()

        # convert pixel values (e.g. center)
        scaled_image = mold_image(image, cfg)
        # convert image into one sample
        sample = expand_dims(scaled_image, 0)

        # make prediction
        yhat = model.detect(sample, verbose=0)[0]

        prediction_time = time.time() - start_time
        if prediction_time < min_time:
          min_time = prediction_time
        if prediction_time > max_time:
          max_time = prediction_time
        count += 1
        total_time += prediction_time
        print("\nImage " + str(i) + " prediction time: " + str(prediction_time))
        # define subplot
        pyplot.subplot(1, 2, 1)
        # turn off axis labels
        pyplot.axis('off')
        # plot raw pixel data
        pyplot.imshow(image)
        pyplot.title('Actual')
        if not custom_image_path:
            # plot masks
            for j in range(mask.shape[2]):
                pyplot.imshow(mask[:, :, j], cmap='gray', alpha=0)
        # get the context for drawing boxes
        pyplot.subplot(1, 2, 2)
        # turn off axis labels
        pyplot.axis('off')
        # plot raw pixel data
        pyplot.imshow(image)
        pyplot.title('Predicted')
        ax = pyplot.gca()
        # plot each box
        for box in yhat['rois']:
            # get coordinates
            y1, x1, y2, x2 = box
            # calculate width and height of the box
            width, height = x2 - x1, y2 - y1
            # create the shape
            rect = Rectangle((x1, y1), width, height, fill=False, color='red')
            # draw the box
            ax.add_patch(rect)

        pyplot.savefig(output_dir_name + "/" + "prediction_" + str(i+1))
        pyplot.clf()
    print("\n\nTotal prediction time for " + str(count) + ": " + str(total_time))
    print("\nAverage prediction time: " + str(total_time/count))
    print("\nTime range: " + str(min_time) + " - " + str(max_time))


def evaluation_preparation(weights_file, default_weights_flag, train_set, test_set, make_pred = False, custom_image_path = None):
        # create config
        cfg = PredictionConfig()
        # define the model
        model = MaskRCNN(mode='inference', model_dir='./', config=cfg)

        if default_weights_flag:
            # Load default weights to check if problem is with training or evaluation
            model.load_weights(weights_file, by_name=True, exclude=["mrcnn_class_logits",
            "mrcnn_bbox_fc", "mrcnn_bbox", "mrcnn_mask"])
        else:
            # load model weights
            model.load_weights(weights_file, by_name=True)

        if not make_pred:
            # Time the detection
            start_time = time.time()

            # evaluate model on training dataset
            train_mAP = evaluate_model(train_set, model, cfg)

            train_prediction_time = time.time() - start_time

            print("\n\nTrain mAP: %.3f" % train_mAP)
            print("\n\n")

            # Time the detection
            start_time = time.time()

            # evaluate model on test dataset
            test_mAP = evaluate_model(test_set, model, cfg)

            test_prediction_time = time.time() - start_time

            print("\n\nTraining set: ")
            print("\nmAP: %.3f" % train_mAP)
            print("\nTotal object detection time: %.3f seconds" % train_prediction_time)
            print("\n\nTest set: ")
            print("\nmAP: %.3f" % test_mAP)
            print("\nTotal object detection time: %.3f seconds" % test_prediction_time)

            with open("evaluation_results.txt", 'a') as fin:
                fin.write("\nWeights: " + weights_file)
                fin.write("\n\n\n\nTrain mAP: " + str(train_mAP))
                fin.write("\nTotal object detection time (s): " + str(train_prediction_time))
                fin.write("\n\nTest mAP: " + str(test_mAP))
                fin.write("\nTotal object detection time (s): " + str(test_prediction_time))

                fin.write("\n***************************************\n\n")

            return train_mAP, test_mAP


        else:
            # plot prediction for custom image
            plot_actual_vs_predicted(test_set, model, cfg, weights_file, IMAGES_TO_PREDICT, custom_image_path)

            return None, None


def save_object(obj, filename):
    with open(filename, 'wb') as outp:  # Overwrites any existing file.
        pickle.dump(obj, outp, pickle.HIGHEST_PROTOCOL)

def get_saved_set(set_file):
  with open(set_file, 'rb') as inp:
        set = pickle.load(inp)
        print('company2: %d' % len(set.image_ids))
        print(type(set))
        return set

def main():
    parser = argparse.ArgumentParser(
        description="Evaluate Mask R-CNN model"
    )

    parser.add_argument(
        "--input",
        "-in",
        help="Input file path.",
        required=True,
    )

    parser.add_argument(
        "--weights",
        "-w",
        action='append',
        help="Weights file path.",
        required=True,
    )

    parser.add_argument(
        "--images",
        "-i",
        help="Number of image folders.",
        required=True,
        type=int,
    )

    parser.add_argument(
        "--train",
        "-t",
        help="Number of training image folders.",
        required=True,
        type=int,
    )

    parser.add_argument(
        "--dimensions",
        "-d",
        help="Maximum Image Dimesnion for training (min: 256).",
        type=int,
    )

    parser.add_argument(
        "--prediction",
        "-p",
        help="Make predictions.",
        type=bool,
        default = False,
    )

    parser.add_argument(
        "--custom_image_path",
        "-ci",
        help="Make prediction on custom image.",
    )

    parser.add_argument(
        "--train_count",
        "-tr",
        help="Number of images to train.",
        type=int,
        default=1000,
    )

    parser.add_argument(
        "--test_count",
        "-te",
        help="Number of images to test.",
        type=int,
        default=200,
    )

    args = parser.parse_args()

    if args.dimensions:
        PredictionConfig.set_max_dim(args.dimensions)

    make_prediction = args.prediction

    if not make_prediction and args.custom_image_path:
      make_prediction = True

    print("**********")
    print("Input confirmation: " + args.input)
    print('Weights confirmation: ')
    for w in args.weights:
      print(w)
    print("**********")


    train_set = WindowDataset()
    train_set.load_dataset(args.input, is_train=True, num_img_dir = args.images, num_train_dir = args.train, count = args.train_count)
    train_set.prepare()
    print('Train: %d images' % len(train_set.image_ids))

    # prepare test/val set
    test_set = WindowDataset()
    test_set.load_dataset(args.input, is_train=False, num_img_dir = args.images, num_train_dir = args.train, count = args.test_count)
    test_set.prepare()
    print('Test: %d images' % len(test_set.image_ids))

    # For eval with multiple weights from command line
    default_weights_flag = False
    for weights_file in args.weights:
        print("\n*********\nUsing weights: " + weights_file + "\n*********\n")

        train_mAP, test_mAP = evaluation_preparation(weights_file, default_weights_flag, train_set, test_set, make_prediction, args.custom_image_path)

if __name__ == "__main__":
    main()
