import subprocess
import sys

def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])


package_list = ["scipy==1.5.2", "numpy==1.18.4", "matplotlib==3.3.4", "pandas==1.1.5", "statsmodels==0.12.2","sklearn","scikit-image","IPython==7.16.3"]

# package_list = ["scipy==1.5.2", "numpy==1.18.4", "matplotlib==3.3.4", "pandas==1.1.5", "statsmodels==0.12.2","sklearn","theano==1.0.4","tensorflow-gpu==1.13.1","keras==2.2.4","scikit-image","IPython==7.16.3"]

for package in package_list:
    install(package)
