import argparse
import logging
import os
import sys
import traceback
from pathlib import Path
from typing import Dict, Any
import numpy as np
import pickle


from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import asarray
from mrcnn.utils import Dataset
from matplotlib import pyplot
from mrcnn.visualize import display_instances
from mrcnn.utils import extract_bboxes
from mrcnn.config import Config
from mrcnn.model import MaskRCNN



from dataset_manager.floor_plan import FloorPlan
from dataset_manager.custom_scripts.render_windows import render_windows
from tqdm import tqdm


class WindowDataset(Dataset):
    def load_dataset(self, is_train  = True, num_img_dir = 5, num_train_dir = 4, count = 200):
        # Define the class
        self.add_class("dataset", 1, "windows")

        panos_to_visualize = ["primary","secondary"]
        geometry_type = "visible"

        id = 0
        file_num = 0

        input_files_list = []
        for i in range(num_img_dir):
            if i == 1456:
                continue
            fname = "../zind_dataset/"
            fname += str(i).zfill(4)+"/zind_data.json"
            input_files_list.append(fname)

        num_failed = 0
        num_success = 0
        failed_tours = []

        description = "Looping through dataset directories (will stop at image count = " + str(count) + ")"

        for input_file in tqdm(input_files_list, desc=description):
            # Try loading and validating the file.

            file_num += 1

            if is_train and file_num >= num_train_dir:
                break
            elif not is_train and file_num < num_train_dir:
                continue

            try:
                zillow_floor_plan = FloorPlan(input_file)
                input_folder = os.path.join(str(Path(input_file).parent))
                for pano_type in panos_to_visualize:
                    panos_list = zillow_floor_plan.panos_layouts[geometry_type][pano_type]

                    for pano_data in panos_list:
                        pano_id = pano_data["pano_id"]


                        pano_image_path = os.path.join(input_folder,  pano_data["image"].split('/')[0], pano_data["image"].split('/')[1])

                        bboxes_pixel_coos, pano_width, pano_height = render_windows(
                            input_folder=zillow_floor_plan.input_folder,
                            pano_data=pano_data
                        )
                        if bboxes_pixel_coos == -1:
                            continue

                        self.add_image('dataset', image_id=id, path=pano_image_path,image_width=pano_width, image_height=pano_height, bboxes=bboxes_pixel_coos)
                        id += 1
                        if id >= count:
                          return


            except Exception as ex:
                print("@@@@@@@@@@@@@@@@@@@@@")
                print(ex)
                failed_tours.append(str(Path(input_file).parent.stem))
                num_failed += 1
                track = traceback.format_exc()
                continue


    # load the masks for an image
    def load_mask(self, image_id):
        # get details of image
        info = self.image_info[image_id]

        boxes = info['bboxes']
        w = info['image_width']
        h = info['image_height']

        # create one array for all masks, each on a different channel
        masks = zeros([h, w, len(boxes)], dtype='uint8')

        # create masks
        class_ids = list()
        count = 0
        for i in range(len(boxes)):
            count += 1
            box = boxes[i]

            if box[2][1]<box[3][1] and box[1][1]>box[0][1]:
                row_s, row_e = int(box[2][1]), int(box[1][1])
            elif box[2][1]<box[3][1] and box[0][1]>box[1][1]:
                row_s, row_e = int(box[2][1]), int(box[0][1])
            elif box[3][1]<box[2][1] and box[1][1]>box[0][1]:
                row_s, row_e = int(box[3][1]), int(box[1][1])
            else:
                row_s, row_e = int(box[3][1]), int(box[0][1])
            if box[3][0] > box[2][0]:
                col_s, col_e = int(box[2][0]), int(box[3][0])
            else:
                col_s, col_e = int(box[3][0]), int(box[2][0])
            masks[row_s:row_e, col_s:col_e, i] = 1

            class_ids.append(self.class_names.index('windows'))


        return masks, asarray(class_ids, dtype='int32')

    # load an image reference
    def image_reference(self, image_id):
        info = self.image_info[image_id]
        return info['path']

# define a configuration for the model
class WindowConfig(Config):
    # define the name of the configuration
    NAME = "window_cfg"
    # number of classes (background + windows)
    NUM_CLASSES = 1 + 1


    @classmethod
    def set_max_dim(cls, image_dim):
        cls.IMAGE_MAX_DIM = image_dim
        print("WindowConfig.IMAGE_MAX_DIM set to " + str(cls.IMAGE_MAX_DIM) + "x" + str(cls.IMAGE_MAX_DIM))

    @classmethod
    def get_max_dim(cls):
        print("WindowConfig.IMAGE_MAX_DIM = " + str(cls.IMAGE_MAX_DIM))


    @classmethod
    def set_steps(cls, steps):
        cls.STEPS_PER_EPOCH = steps
        print("WindowConfig.STEPS_PER_EPOCH set to " + str(cls.STEPS_PER_EPOCH))

    @classmethod
    def get_steps(cls):
        print("WindowConfig.STEPS_PER_EPOCH = " + str(cls.STEPS_PER_EPOCH))

    @classmethod
    def set_lr(cls, lr):
        cls.LEARNING_RATE = lr
        print("WindowConfig.LEARNING_RATE set to " + str(cls.LEARNING_RATE))

    @classmethod
    def get_lr(cls):
        print("WindowConfig.LEARNING_RATE = " + str(cls.LEARNING_RATE))


def main():
    parser = argparse.ArgumentParser(
        description="Visualize & validate Zillow Indoor Dataset (ZInD)"
    )

    parser.add_argument(
        "--weights",
        "-w",
        help="Pretrained weights.",
    )

    parser.add_argument(
        "--learningrate",
        "-lr",
        help="Learning Rate.",
        type=float,
    )

    parser.add_argument(
        "--images",
        "-i",
        help="Number of image folders.",
        required=True,
        type=int,
    )

    parser.add_argument(
        "--train",
        "-t",
        help="Number of training image folders.",
        required=True,
        type=int,
    )

    parser.add_argument(
        "--dimensions",
        "-d",
        help="Maximum Image Dimesnion for training (min: 256).",
        type=int,
    )

    parser.add_argument(
        "--epochs",
        "-e",
        help="Number of epochs.",
        type=int,
        default = 5,
    )

    parser.add_argument(
        "--steps",
        "-s",
        help="Steps per epoch.",
        type=int,
    )

    parser.add_argument(
        "--train_count",
        "-tr",
        help="Number of images to train.",
        type=int,
        default=1000,
    )

    parser.add_argument(
        "--test_count",
        "-te",
        help="Number of images to test.",
        type=int,
        default=200,
    )

    args = parser.parse_args()

    if args.dimensions:
        WindowConfig.set_max_dim(args.dimensions)
    if args.steps:
        WindowConfig.set_steps(args.steps)
    if args.learningrate:
        WindowConfig.set_lr(args.learningrate)

    print("******************************* LETS BEGIN ***********************************")

    train_set = WindowDataset()
    train_set.load_dataset(is_train=True, num_img_dir = args.images, num_train_dir = args.train, count = args.train_count)
    train_set.prepare()
    print('Train: %d images' % len(train_set.image_ids))


    test_set = WindowDataset()
    test_set.load_dataset(is_train=False, num_img_dir = args.images, num_train_dir = args.train, count = args.test_count)
    test_set.prepare()
    print('Test: %d images' % len(test_set.image_ids))
    # TRAIN
    # prepare config
    config = WindowConfig()
    config.display()
    # define the model
    model = MaskRCNN(mode='training', model_dir='./', config=config)
    if args.weights == "random":
      print("\nUsing random weight intialization\n")
    elif args.weights:
      # load custom weights
      model.load_weights(args.weights, by_name=True)
    else:
      # load weights (mscoco) and exclude the output layers
      model.load_weights('mask_rcnn_coco.h5', by_name=True, exclude=["mrcnn_class_logits",
      "mrcnn_bbox_fc", "mrcnn_bbox", "mrcnn_mask"])

    # train weights (output layers or 'heads')
    model.train(train_set, test_set, learning_rate=config.LEARNING_RATE, epochs=args.epochs, layers='heads')

    print('\n\n\nTrain: %d' % len(train_set.image_ids))
    print('Test: %d' % len(test_set.image_ids))

    # # black and white
    # model.train(train_set, test_set, learning_rate=config.LEARNING_RATE, args.epochs, layers='heads')




if __name__ == "__main__":
    main()
