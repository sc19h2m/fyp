"""
This module contains the window rendering route.
"""
import argparse
import itertools
import logging
import os
import sys
from typing import List, Dict, Any

from dataset_manager.custom_scripts.render_windows import visualiser


def main():
    parser = argparse.ArgumentParser(
        description="Render windows on ZiND data."
    )

    parser.add_argument(
        "--input",
        "-i",
        help="Input folder.",
        required=True,
        type=str,
    )

    parser.add_argument(
        "--output",
        "-o",
        help="Output folder.",
        required=True,
        type=str,
    )

    args = parser.parse_args()

    print(str(args.input))
    fname = args.input + "/zind_data.json"
    fout = args.output

    visualiser(fname, fout)


if __name__ == "__main__":
    main()
