from PIL import Image
import cv2
import numpy as np

# Overlay an image
def overlay_image(window_img, bboxes, curtain_choice):
    print('\nOverlaying image...')
    # Add the alpha channel to the image
    window_img = cv2.cvtColor(window_img, cv2.COLOR_BGR2RGBA)
    index = 1

    # Offset variable values
    x_offset_value = 20
    y_offset_value = 20
    length_offset = 50

    # Computation based on user selection
    if curtain_choice == 0:
        curtain_img = cv2.imread("curtains/blue_curt.png", -1)
    elif curtain_choice == 1:
        x_offset_value = 100
        y_offset_value = 100
        curtain_img = cv2.imread("curtains/white_curt.png", -1)
    elif curtain_choice == 2:
        curtain_img = cv2.imread("curtains/black_curt.png", -1)

    # Iterate through the predicted bounding boxes
    for bbox in bboxes:
        # Skip if the coordinates are faulty
        if bbox[0] + bbox [1] < 1:
            continue
        print('Processing bounding box ' + str(index))
        index += 1

        # Fill a list with bounding box coordinates
        coordinates = []
        for i in range(4):
            coordinates.append(int(bbox[i]))

        # Computes curtain size
        curtain_size_x = abs(coordinates[2]-coordinates[0]) + (x_offset_value * 2)
        curtain_size_y = abs(coordinates[3]-coordinates[1]) + (y_offset_value * 2) + length_offset

        # Resize the curtain png and add the alpha channnel
        curtain_img = cv2.resize(curtain_img, (curtain_size_x, curtain_size_y))
        curtain_img = cv2.cvtColor(curtain_img, cv2.COLOR_RGB2RGBA)

        # Compute the overlay variables
        x_offset=coordinates[0] - x_offset_value
        y_offset=coordinates[1] - y_offset_value
        x1, x2 = x_offset, coordinates[2] + x_offset_value
        y1, y2 = y_offset, coordinates[3] + y_offset_value + length_offset

        # Account for out of bound points which may have been caused by higher offset values and
        # Generate normalised curtain masks
        if y1 < 0:
            curtain_mask = curtain_img[:, abs(coordinates[3]-coordinates[1]) + y1, 3] / 255.0
            y1 = 0
        else:
            curtain_mask = curtain_img[:, :, 3] / 255.0

        inverse_curtain_mask = 1.0 - curtain_mask

        # loop through the channels and overlay the image
        for c in range(0, 3):
            window_img[y1:y2, x1:x2, c] = (curtain_mask * curtain_img[:, :, c] + inverse_curtain_mask * window_img[y1:y2, x1:x2, c])

    # Convert the image back to RGB
    window_img = cv2.cvtColor(np.array(window_img), cv2.COLOR_RGBA2RGB)
    print('Image successfully overlayed and converted back to RGB\n')
    return window_img
