from flask import Flask, render_template , request , jsonify
from PIL import Image
import os , io , sys
import numpy as np
import cv2
import base64
import time

import importlib
import detect_object
from window_processor import overlay_image

app = Flask(__name__)

# Global varibale for curtain choice [0: blue, 1: white, 2: black]
curtain_choice = 0
# Global varibale for image processing status
image_processed = False
# Global varibale for the YOLOv4 model
saved_model_loaded = None


#Curtain choice processes
@app.route('/set_curtain_to_blue')
def set_curtain_to_blue():
	global curtain_choice
	curtain_choice = 0
	print ("\nBlue curtain fabric selected\n")
	return ("nothing")
@app.route('/set_curtain_to_white')
def set_curtain_to_white():
	global curtain_choice
	curtain_choice = 1
	print ("\nWhite curtain fabric selected\n")
	return ("nothing")
@app.route('/set_curtain_to_black')
def set_curtain_to_black():
	global curtain_choice
	curtain_choice = 2
	print ("\nBlack curtain fabric selected\n")
	return ("nothing")

# The main route, it calls the bounding box predicter and image overlayer methods.
@app.route('/process_image' , methods=['POST'])
def process_image():
	# Load and prepare image
	file = request.files['image'].read()
	npimg = np.fromstring(file, np.uint8)
	img = cv2.imdecode(npimg,cv2.IMREAD_COLOR)

	# Making predictions and overlay curtains
	pred_box = detect_object.window_detector(img, saved_model_loaded)
	img = overlay_image(img, pred_box, curtain_choice)

	# Post-processing
	img = Image.fromarray(img.astype("uint8"))
	rawBytes = io.BytesIO()
	img.save(rawBytes, "JPEG")
	rawBytes.seek(0)
	img_base64 = base64.b64encode(rawBytes.read())
	return jsonify({'status':str(img_base64)})

# Index route
@app.route('/')
def home():
	# Load YOLOv4 model
	print('\nLoading YOLOv4 model...')
	global saved_model_loaded
	start_time = time.time()
	saved_model_loaded = detect_object.load_model()
	prediction_time = time.time() - start_time
	print('\nModel loaded successfully in ' + str(prediction_time) + ' seconds.\n')
	global image_processed
	return render_template('./index.html', image_processed=image_processed)


@app.after_request
def after_request(response):
	# print("log: setting cors" , file = sys.stderr)
	response.headers.add('Access-Control-Allow-Origin', '*')
	response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
	response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
	return response


if __name__ == '__main__':
	app.run(debug = False)
